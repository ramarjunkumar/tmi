import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PayPage } from '../pay/pay';
import { TaxPage } from '../tax/tax';
import { CtcPage } from '../ctc/ctc';
import { ProfilePage } from '../profile/profile';
import { HomePage } from '../home/home';
import { LogoutProvider } from '../../providers/logout/logout';
import { User } from '../login/user.model';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  public clientname: string;
  public name: string;
  public randumnum: string;
  public ram: string;
  user = new User('', '');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public logoutProvider: LogoutProvider,
    public alertCtrl: AlertController,
    private storage: Storage) 
  {
    this.clientname = navParams.get('client');
    this.name = navParams.get('name');
    this.randumnum = navParams.get('randumnum');
    console.log(1, this.clientname, this.name, this.randumnum);
    this.storage.set('username', this.name);
    this.storage.set('client', this.clientname);
    this.storage.set('randomNumber', this.randumnum);
    let uname =  this.storage.get('username').then((val) => {
      console.log('Your  is', val);
      this.ram = val;
    });
    let Cname =  this.storage.get('client').then((val) => {
      console.log(' age is', val);
      this.ram = val;
    });
    let rname =  this.storage.get('randomNumber').then((val) => {
      console.log('Your age ', val);
      this.ram = val;
    });
    this.storage.set('hai', this.name);
    console.log(this.storage.get('hai').then((val) => {
      console.log('Your age is', val);
      this.ram = val;
    }), 'new');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  // goPayPage() {
  // 	this.navCtrl.push(PayPage);
  // }

  goTaxPage() {
  	this.navCtrl.push(TaxPage);
  }

  goCtcPage() {
  	this.navCtrl.push(CtcPage);
  }

  goProfilePage() {
    this.navCtrl.push(ProfilePage);
  }

  goHomePage() {
    this.navCtrl.push(HomePage);
  }

  ngOnInit() {
  }

  navigate(clientname, name, randumnum): void{
    console.log(this.ram, 'you');
    this.navCtrl.push(PayPage, {
      clientname: this.clientname,
      randomNumber: this.randumnum,
      username: this.name
    });
  }

  navigateCtcPage(clientname, name, randumnum): void{
      console.log(5, this.clientname)
    this.navCtrl.push(CtcPage, {
      clientname: this.clientname,
      randomNumber: this.randumnum,
      username: this.name
    });
  }

  navigateTaxPage(clientname, name, randumnum): void{
      console.log(5, this.clientname)
    this.navCtrl.push(TaxPage, {
      clientname: this.clientname,
      randomNumber: this.randumnum,
      username: this.name
    });
  }

  navigateProfilePage(clientname, name, randumnum): void{
      console.log(5, this.clientname)
    this.navCtrl.push(ProfilePage, {
      clientname: this.clientname,
      randomNumber: this.randumnum,
      username: this.name
    });
  }

  showLogoutOkAlert() {
    let alert = this.alertCtrl.create({
      title: 'Are you Sure want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.push(HomePage);
          }
        }
      ]
    });
    alert.present();
  }

  showErrorLogoutAlert() {
    const alert = this.alertCtrl.create({
      title: 'Failed to logout',
      buttons: ['OK']
    });
    alert.present();
  }

  logout(): void {
    this.user.companyName = this.clientname;
    this.user.username = this.name;
    this.user.randomNumber = this.randumnum;
    this.logoutProvider.logout(this.user).
    then((data: any) => 
    {
      const result = JSON.parse(data);
      
      if (result && result[0].RESULT === "Sucess") {
        this.randumnum = result[0].RANDOMNUMBER
        this.showLogoutOkAlert();
      }
      else {
        this.showErrorLogoutAlert();
      }
    })       
  }
}
