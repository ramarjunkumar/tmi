import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { ResetpasswordPage } from '../resetpassword/resetpassword';
import CryptoJS from "crypto-js";

import { LoginProvider } from '../../providers/login/login';
import { User } from './user.model';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
// import { PayPage } from '../pay/pay';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = new User('', '');
  public clientname: string;
  public companyName: string;
  public username: string;
  public randomNumber: string;
  public userpassword: string;
  public encpassword: string;

  public cname:string;
  public uname:string;
  public rnumber:string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loginProvider: LoginProvider,
    public alertCtrl: AlertController,
    private storage: Storage) {
      this.clientname = navParams.get('clientname');
  }

  ngOnInit() {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goDashboard() {
  	this.navCtrl.push(DashboardPage);
  }

  goResetPassword() {
    this.navCtrl.push(ResetpasswordPage);
  }

  showLogoutAlert() {
    const alert = this.alertCtrl.create({
      title: 'Invalid username or password',
      buttons: ['OK']
    });
    alert.present();
  }

  onSubmit(value: any): void {
    this.user.companyName = this.clientname;
    this.encrypt();
    this.user.password = this.userpassword;
    this.loginProvider.login(this.user).
    then((data: any) => 
    {
      const result = JSON.parse(data);
      this.companyName = this.user.companyName;
      this.username = this.user.username;
      console.log(this.username, 'hwwwwwwww')
      this.storage.set('comname', this.companyName);
      this.storage.get('comname').then((val) => {
        console.log('comname', val);
        this.cname = val;
      });

      if (result && result[0].RESULT === "Sucess") {
        this.randomNumber = result[0].RANDOMNUMBER;
        console.log(this.randomNumber, 'number');
        console.log(result);
        console.log(this.username, 'doubt');
        this.storage.set('uname', this.username);
        this.storage.get('uname').then((val) => {
          console.log('uname', val);
          this.uname = val;
        });
        this.storage.set('randomNumber', this.randomNumber);
        this.storage.get('randomNumber').then((num) => {
          console.log('randomNumber', num);
          this.rnumber = num;
        });
        // localStorage.setItem('username', this.username);
        // localStorage.setItem('password', this.user.password);
        // localStorage.setItem('randomNumber', this.randomNumber);
        // let uname =  localStorage.getItem('username');
        // console.log(localStorage.getItem('password'),231);
        // let rnumber = localStorage.getItem('randomNumber');
        // this.navigate(this.cname, this.rnumber, this.uname);
          this.navCtrl.push(DashboardPage, {
            client: this.cname,
            randumnum: this.rnumber,
            name: this.uname
          });
      }
      else {
        this.showLogoutAlert();
        this.user.password = '';
      }
    })      
  }

    encrypt(): void {
        var text = this.user.password;
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
 
        var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), 
        key,
        { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        this.userpassword = CryptoJS.enc.Utf8.parse(encryptedlogin);
    }

    // navigate(companyName, randumNumber, username): void{
    //   this.navCtrl.push(DashboardPage, {
    //     client: companyName,
    //     randumnum: randumNumber,
    //     name: username
    //   });
    // }

}

