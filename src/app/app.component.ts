import { Component, ViewChild } from '@angular/core';
import { Platform, Events, ToastController, App, Nav} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/resetpassword';
import { PayPage } from '../pages/pay/pay';
import { TaxPage } from '../pages/tax/tax';
import { CtcPage } from '../pages/ctc/tc';
import { ProfilePage } from '../pages/profle/profile';
import { ResetpasswordPage } from '../pages/resetpassword/resetpassword';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { NetworkProvider } from '../providers/network/network';
import { setTimeout } from 'timers';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;
  public username: string;
  public companyName: string;
  public randomNumber: string;

  constructor(private platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private storage: Storage,
              public network: Network,
              public networkProvider: NetworkProvider,
              public events: Events,
              public toast: ToastController,
              public  app: App) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      statusBar.backgroundColorByHexString('#430091');
      splashScreen.hide();
    });
    this.network.onConnect().subscribe(() =>{
      this.toast.create({
        message: 'Network connected',
        duration: 3000
      }).present();
    });
    this.network.onDisconnect().subscribe(() => {
      this.toast.create({
        message: 'Network Disconnected',
        duration: 3000
      }).present();
    });
     this.storage.get('username').then((val) => {
        this.username=val;
    });
      this.storage.get('companyName').then((val) => {
        this.companyName=val;

    });
      this.storage.get('randomNumber').then((val) => {
        this.randomNumber=val;
      this.checkPreviousAuthorization();
      this.initializeApp();
    });
  }

  initializeApp(): void  {
      this.platform.registerBackButtonAction(() => {
        // Catches the active view
        let nav = this.app.getActiveNavs()[0];
        let activeView = nav.getActive();     
        console.log(activeView.name, '123');
        // Checks if can go back before show up the alert
          if(activeView.name === 'HomePage' || activeView.name === 'DashboardPage') {
            this.platform.exitApp();
          }
          else if(activeView.name === 'TaxPage'){
            nav.pop();             
          }
          else if(activeView.name === 'CtcPage'){
            nav.pop();           
          }
          else if(activeView.name === 'ProfilePage'){
            nav.pop();             
          }
          else if(activeView.name === 'ResetPasswordPage') {
            nav.setRoot('ProfilePage');
          }
          else if(activeView.name === 'LoginPage') {
            nav.pop();
          }
          else {
            nav.pop(); 
          }
      });
  }

  checkPreviousAuthorization(): void { 
    if((this.randomNumber === undefined ||this.randomNumber === null)) {
      setTimeout(() => {
        this.rootPage = HomePage;
      }, 200);
    } else {
      this.rootPage = DashboardPage;
    }
  }
}
