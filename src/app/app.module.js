var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ResetpasswordPage } from '../pages/resetpassword/resetpassword';
import { PayPage } from '../pages/pay/pay';
import { TaxPage } from '../pages/tax/tax';
import { CtcPage } from '../pages/ctc/ctc';
import { ProfilePage } from '../pages/profile/profile';
import { LoginProvider } from '../providers/login/login';
import { ClientProvider } from '../providers/client/client';
import { PayProvider } from '../providers/pay/pay';
import { CtcProvider } from '../providers/ctc/ctc';
import { TaxProvider } from '../providers/tax/tax';
import { ProfileProvider } from '../providers/profile/profile';
import { LogoutProvider } from '../providers/logout/logout';
import { ResetProvider } from '../providers/reset/reset';
// import { EqualValidator } from '../pages/resetpassword/equal-validator.directive';
import { IonicStorageModule } from '@ionic/storage';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                HomePage,
                RegisterPage,
                LoginPage,
                DashboardPage,
                ResetpasswordPage,
                PayPage,
                TaxPage,
                CtcPage,
                ProfilePage
            ],
            imports: [
                BrowserModule,
                HttpModule,
                HttpClientModule,
                ReactiveFormsModule,
                IonicModule.forRoot(MyApp),
                IonicStorageModule.forRoot()
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                HomePage,
                RegisterPage,
                LoginPage,
                DashboardPage,
                ResetpasswordPage,
                PayPage,
                TaxPage,
                CtcPage,
                ProfilePage
            ],
            providers: [
                StatusBar,
                SplashScreen,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                LoginProvider,
                ClientProvider,
                PayProvider,
                CtcProvider,
                TaxProvider,
                ProfileProvider,
                LogoutProvider,
                ResetProvider
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map