import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, NavController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ResetpasswordPage } from '../pages/resetpassword/resetpassword';
import { PayPage } from '../pages/pay/pay';
import { TaxPage } from '../pages/tax/tax';
import { CtcPage } from '../pages/ctc/ctc';
import { ProfilePage } from '../pages/profile/profile';
import { LoginProvider } from '../providers/login/login';
import { ClientProvider } from '../providers/client/client';
import { PayProvider } from '../providers/pay/pay';
import { CtcProvider } from '../providers/ctc/ctc';
import { TaxProvider } from '../providers/tax/tax';
import { ProfileProvider } from '../providers/profile/profile';
import { LogoutProvider } from '../providers/logout/logout';
import { ResetProvider } from '../providers/reset/reset';
import { IonicStorageModule } from '@ionic/storage';
import { NetworkProvider } from '../providers/network/network';
import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RegisterPage,
    LoginPage,
    DashboardPage,
    ResetpasswordPage,
    PayPage,
    TaxPage,
    CtcPage,
    ProfilePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RegisterPage,
    LoginPage,
    DashboardPage,
    ResetpasswordPage,
    PayPage,
    TaxPage,
    CtcPage,
    ProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LoginProvider,
    ClientProvider,
    PayProvider,
    CtcProvider,
    TaxProvider,
    ProfileProvider,
    LogoutProvider,
    ResetProvider,
    NetworkProvider
  ]
})
export class AppModule {}
