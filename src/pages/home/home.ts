import { Component } from '@angular/core';
import { NavController, LoadingController, Platform } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { LoginPage } from '../login/login';

import { Client } from './client.model';
import { ClientProvider } from '../../providers/client/client';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  client = new Client('');
  public clientcode: string;

  constructor(
    public navCtrl: NavController,
    public clientProvider: ClientProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private platform: Platform
  ) {
  }

  goLogin() {
  	this.navCtrl.push(LoginPage);
  }

  goRegister() {
  	this.navCtrl.push(RegisterPage);
  }

  ionViewDidEnter() {
    this.platform.registerBackButtonAction(() => {
      this.platform.exitApp();
    });
  }

  showwrongAlert() {
    const alert = this.alertCtrl.create({
      title: 'Invalid Clientcode',
      buttons: ['OK']
    });
    alert.present();
  }

  onSubmit(): void {
    let loader = this.loadingCtrl.create({
    });
    loader.present();
    this.clientProvider.clientLogin(this.client).
    then((data: any) => 
    {
      const result = JSON.parse(data);
      this.clientcode = this.client.clientcode
      if (result && result[0].RESULT === "Sucess") {
        loader.dismiss();
        this.navigate(this.clientcode);
      }
      else {
        loader.dismiss();
        this.showwrongAlert();
      }
    })      
  }
  

  navigate(clientcode): void{
    this.navCtrl.push(LoginPage, {
      clientname: clientcode
    });
  }

}
