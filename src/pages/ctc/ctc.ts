import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { CtcProvider } from '../../providers/ctc/ctc';
import { Ctc } from './ctc.model';

@IonicPage()
@Component({
  selector: 'page-ctc',
  templateUrl: 'ctc.html',
})
export class CtcPage {

  ctc = new Ctc('', '');
  ctcs: Ctc[] = [];
  clientname: string;
  username: string;
  randumnum: string;
  public isSubmitted: boolean = false;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private ctcProvider: CtcProvider,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController
            )  {
    this.clientname = navParams.get('clientname');
    this.username = navParams.get('username');
    this.randumnum = navParams.get('randomNumber');
    console.log('ctc', this.clientname, this.username, this.randumnum);
    this.onSubmit();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ctcPage');
  }

  alert() {
    const alert = this.alertCtrl.create({
      title: 'Ctc is not enabled for the Client',
      buttons: ['OK']
    });
    alert.present();
  }

  onSubmit(): void {
    let loader = this.loadingCtrl.create({
    });
    this.ctc.companyName = this.clientname;
    this.ctc.username = this.username;
    this.ctc.randomNumber = this.randumnum;
    loader.present();
    this.ctcProvider.getctc(this.ctc).
    then((data: any) => 
    {
      const result = JSON.parse(data);
      console.log(result);
      console.log(result.length);
      if(result){
        this.ctcs = result;
        loader.dismiss();
        this.isSubmitted = true;
      }
      else {
        this.alert();
      }
    })      
  }

}
