import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CtcPage } from './ctc';

@NgModule({
  declarations: [
    CtcPage,
  ],
  imports: [
    IonicPageModule.forChild(CtcPage),
  ],
})
export class CtcPageModule {}
