export class Ctc {
    constructor(
        public username?: string,
        public randomNumber?: string,
        public companyName?: string,
        public COMNAME?: string,
        public ANNUAL?: string,
        public MONTHLY?: string,
        public YEARLY?: string
    ) {}
}
