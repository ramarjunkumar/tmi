var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ResetpasswordPage } from '../resetpassword/resetpassword';
import { ProfileProvider } from '../../providers/profile/profile';
import { Profile } from './profile.model';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams, profileProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.profileProvider = profileProvider;
        this.profile = new Profile('', '');
        this.profiles = [];
        this.clientname = navParams.get('clientname');
        this.username = navParams.get('username');
        this.randumnum = navParams.get('randomNumber');
        console.log('profile', this.clientname, this.username, this.randumnum);
        this.onSubmit();
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.goResetPassword = function () {
        this.navCtrl.push(ResetpasswordPage);
    };
    ProfilePage.prototype.onSubmit = function () {
        var _this = this;
        this.profile.companyName = this.clientname;
        this.profile.username = this.username;
        this.profile.randomNumber = this.randumnum;
        this.profileProvider.getProfile(this.profile).
            then(function (data) {
            var result = JSON.parse(data);
            _this.profiles = result;
            console.log(_this.profiles);
        });
    };
    ProfilePage.prototype.navigateResetPage = function (clientname, username, randumnum) {
        console.log(5, this.clientname);
        this.navCtrl.push(ResetpasswordPage, {
            clientname: this.clientname,
            randomNumber: this.randumnum,
            username: this.username
        });
    };
    ProfilePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-profile',
            templateUrl: 'profile.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            ProfileProvider])
    ], ProfilePage);
    return ProfilePage;
}());
export { ProfilePage };
//# sourceMappingURL=profile.js.map