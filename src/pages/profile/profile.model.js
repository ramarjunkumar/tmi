var Profile = /** @class */ (function () {
    function Profile(month, year, username, randomNumber, companyName, NAME, EMPLOYEE_ID, DESIGNATION, BRAND, LOCATION, DATE_OF_JOINING, PAN_CARD_NUMBER) {
        this.month = month;
        this.year = year;
        this.username = username;
        this.randomNumber = randomNumber;
        this.companyName = companyName;
        this.NAME = NAME;
        this.EMPLOYEE_ID = EMPLOYEE_ID;
        this.DESIGNATION = DESIGNATION;
        this.BRAND = BRAND;
        this.LOCATION = LOCATION;
        this.DATE_OF_JOINING = DATE_OF_JOINING;
        this.PAN_CARD_NUMBER = PAN_CARD_NUMBER;
    }
    return Profile;
}());
export { Profile };
//# sourceMappingURL=profile.model.js.map