export class Profile {
    constructor(
        public month: string,
        public year: string,
        public username?: string,
        public randomNumber?: string,
        public companyName?: string,
        public NAME?: string,
        public EMPLOYEE_ID?: string,
        public DESIGNATION?: string,
        public BRAND?: string,
        public LOCATION?: string,
        public DATE_OF_JOINING?: string,
        public PAN_CARD_NUMBER?: string,
        public DATE_OF_BIRTH?: string,
        public REPORTING_MANAGER?: string
    ) {}
}
