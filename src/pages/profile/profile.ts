import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ResetpasswordPage } from '../resetpassword/resetpassword';
import { ProfileProvider } from '../../providers/profile/profile';
import { Profile } from './profile.model';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  profile = new Profile('', '');
  profiles: Profile[] = [];
  clientname: string;
  username: string;
  randumnum: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private profileProvider: ProfileProvider,
              public loadingCtrl: LoadingController
            )  {
    this.clientname = navParams.get('clientname');
    this.username = navParams.get('username');
    this.randumnum = navParams.get('randomNumber');
    console.log('profile', this.clientname, this.username, this.randumnum);
    this.onSubmit();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  goResetPassword() {
    this.navCtrl.push(ResetpasswordPage);
  }

  onSubmit(): void {
    let loader = this.loadingCtrl.create({
    });
    this.profile.companyName = this.clientname;
    this.profile.username = this.username;
    this.profile.randomNumber = this.randumnum;
    loader.present();
    this.profileProvider.getProfile(this.profile).
    then((data: any) => 
    { 
      const result = JSON.parse(data);
      this.profiles = result;
      loader.dismiss();
    })      
  }

  navigateResetPage(clientname, username, randumnum): void{
    console.log(5, this.clientname)
    this.navCtrl.push(ResetpasswordPage, {
    clientname: this.clientname,
    randomNumber: this.randumnum,
    username: this.username
  });
}

}
