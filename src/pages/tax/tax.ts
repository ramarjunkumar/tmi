import { Component, OnInit, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { TaxProvider } from '../../providers/tax/tax';
import { Tax } from './tax.model';

@IonicPage()
@Component({
  selector: 'page-tax',
  templateUrl: 'tax.html',
})
export class TaxPage implements OnInit {

  tax = new Tax('', '');
  taxs: Tax[] = [];
  clientname: string;
  username: string;
  randumnum: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private taxProvider: TaxProvider,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public elementRef: ElementRef
            )  {
    this.clientname = navParams.get('clientname');
    this.username = navParams.get('username');
    this.randumnum = navParams.get('randomNumber');
    console.log('tax', this.clientname, this.username, this.randumnum);
    this.onSubmit();
  }

  ngOnInit() {
    // const acc = this.elementRef.nativeElement.querySelector('.accordion');
    
    // for (let i = 0; i < acc.length; i++) {
    //   acc[i].addEventListener("click", function() {
    //     this.classList.toggle("active");
    //     var panel = this.nextElementSibling;
    //     if (panel.style.maxHeight){
    //       panel.style.maxHeight = null;
    //     } else {
    //       panel.style.maxHeight = panel.scrollHeight + "px";
    //     } 
    //   });
    // }
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad taxPage');
  }

  alert() {
    const alert = this.alertCtrl.create({
      title: 'No Results found',
      buttons: ['OK']
    });
    alert.present();
  }

  onSubmit(): void {
    let loader = this.loadingCtrl.create({
    });
    this.tax.companyName = this.clientname;
    this.tax.username = this.username;
    this.tax.randomNumber = this.randumnum;
    loader.present();
    this.taxProvider.gettax(this.tax).
    then((data: any) => 
    {
      const result = JSON.parse(data);
      console.log(result);
      console.log(result.length, '123'); 
      if(result){
        this.taxs = result;
        loader.dismiss();
      }else {
        this.alert();
      }
    })      
  }
}
