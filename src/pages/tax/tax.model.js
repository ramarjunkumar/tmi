var Tax = /** @class */ (function () {
    function Tax(username, randomNumber, companyName, S_DESCRIPTION, N_ACTUAL_AMOUNT, N_TAXABLE_AMOUNT, GROUP_DETAILS) {
        this.username = username;
        this.randomNumber = randomNumber;
        this.companyName = companyName;
        this.S_DESCRIPTION = S_DESCRIPTION;
        this.N_ACTUAL_AMOUNT = N_ACTUAL_AMOUNT;
        this.N_TAXABLE_AMOUNT = N_TAXABLE_AMOUNT;
        this.GROUP_DETAILS = GROUP_DETAILS;
    }
    return Tax;
}());
export { Tax };
//# sourceMappingURL=tax.model.js.map