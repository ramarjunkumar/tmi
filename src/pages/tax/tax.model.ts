export class Tax {
    constructor(
        public username?: string,
        public randomNumber?: string,
        public companyName?: string,
        public S_DESCRIPTION?: string,
        public N_ACTUAL_AMOUNT?: string,
        public N_TAXABLE_AMOUNT?: string,
        public GROUP_DETAILS?: string
    ) {}
}
