var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaxProvider } from '../../providers/tax/tax';
import { Tax } from './tax.model';
var TaxPage = /** @class */ (function () {
    function TaxPage(navCtrl, navParams, taxProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.taxProvider = taxProvider;
        this.tax = new Tax('', '');
        this.taxs = [];
        this.clientname = navParams.get('clientname');
        this.username = navParams.get('username');
        this.randumnum = navParams.get('randomNumber');
        console.log('tax', this.clientname, this.username, this.randumnum);
        this.onSubmit();
    }
    TaxPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad taxPage');
    };
    TaxPage.prototype.onSubmit = function () {
        var _this = this;
        this.tax.companyName = this.clientname;
        this.tax.username = this.username;
        this.tax.randomNumber = this.randumnum;
        this.taxProvider.gettax(this.tax).
            then(function (data) {
            var result = JSON.parse(data);
            _this.taxs = result;
            console.log(_this.taxs);
        });
    };
    TaxPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-tax',
            templateUrl: 'tax.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            TaxProvider])
    ], TaxPage);
    return TaxPage;
}());
export { TaxPage };
//# sourceMappingURL=tax.js.map