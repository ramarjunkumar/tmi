import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { FormGroup, Validators, FormBuilder} from '@angular/forms';
import { CustomValidators } from './custom-validators';
import { Reset } from './resetpassword.model';
import { ResetProvider } from '../../providers/reset/reset';
import CryptoJS from "crypto-js";
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-resetpassword',
  templateUrl: 'resetpassword.html',
})
export class ResetpasswordPage {

  reset = new Reset();
  clientname: string;
  username: string;
  randumnum: string;
  public changePasswordForm: FormGroup;
  public isFormSubmitted: boolean;
  public confirmpassword: string;
  public userresetassword: string;
  public newpassword: string;

  public formErrors = {
    'new_password': '',
    'confirm_password': '',
    'password' : ''
  };
  private validationMessages = {
    'password': {
      'required': 'Password is required'
    },
    'new_password': {
      'required': 'New password is required',
      'pattern': 'Password should be combination of alpha numeric i.e. a mix of alphabets {at-least one lower and upper case} and numbers',
      'minlength': 'Password should contain minimum 6 and maximum 10 letters',
      'maxlength': 'Password should contain minimum 6 and maximum 10 letters' 
    },
    'confirm_password': {
      'required': 'Password confirmation required',
      'mismatch': 'Password does not match confirmation password'
    }
  };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,              
              private resetProvider: ResetProvider,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController
            ) {
              this.clientname = navParams.get('clientname');
              this.username = navParams.get('username');
              this.randumnum = navParams.get('randomNumber');
              console.log('reset', this.clientname, this.username, this.randumnum);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetpasswordPage');
  }

  goDashboard() {
    this.navCtrl.push(DashboardPage);
  }

  ngOnInit() {
    this.buildForm();
  }


  public buildForm(): void {
    this.changePasswordForm = this.formBuilder.group({   
      'password': [
        this.reset.password, [
          Validators.required
        ]
      ],
      'new_password': [
        this.reset.new_password,
        Validators.compose([
        Validators.required,
        Validators.pattern(`.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*`),
        Validators.minLength(6),
        Validators.maxLength(10)])
      ],
      'confirm_password': [
        this.reset.confirm_password,
        Validators.required,
      ],
     }, {
       validator: CustomValidators.passwordMatchValidator // Inject the provider method
     });
     this.changePasswordForm.valueChanges.subscribe(data => this.onValueChanged(data));

     this.onValueChanged();
   }

   private onValueChanged(data?: any) {
    if (!this.changePasswordForm) { return; }
    const form = this.changePasswordForm;
    console.log(11);

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && (this.isFormSubmitted || control.dirty) && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
          console.log(122222);
        }
      }
    }
  }

  showPassowrdAlert() {
    const alert = this.alertCtrl.create({
      title: 'Password changed successfully',
      buttons: ['OK']
    });
    alert.present();
  }

  showLogoutAlert() {
    const alert = this.alertCtrl.create({
      title: 'Invalid password or new password',
      buttons: ['OK']
    });
    alert.present();
  }
 
  onSubmit(): void {
    let loader = this.loadingCtrl.create({
    });
    this.reset = this.changePasswordForm.value;
    this.reset.companyName = this.clientname;
    this.reset.username = this.username;
    this.reset.randomNumber = this.randumnum;
    this.reset.password = this.encryptPassword(this.reset.password);
    this.reset.new_password = this.encryptPassword(this.reset.new_password);
    loader.present();
    this.resetProvider.resetPassword(this.reset).
    then((data: any) => 
    {
      const result = JSON.parse(data);      
      if(result && result[0].RESULT === "Success") {
        loader.dismiss();
        this.showPassowrdAlert();
        this.navCtrl.push(DashboardPage);
      }
      else {
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: result[0].MESSAGE,
          buttons: ['OK']
        });
        alert.present();
        this.reset.password = '';
        this.reset.new_password = '';
        this.reset.confirm_password = '';
      }
    })     
  }

  ionViewWillLeave() {
    console.log('hai');
  }

  encryptPassword(text): string {
    console.log(text);
    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

    var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), 
    key,
    { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
    return CryptoJS.enc.Utf8.parse(encryptedlogin);
  }
}
