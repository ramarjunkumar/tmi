import { FormGroup, AbstractControl } from '@angular/forms';
import { ValidationError } from "./validation-error";

export class CustomValidators {
  static passwordMatchValidator(group: FormGroup): ValidationError {
    let new_password : AbstractControl = group.get('new_password');
    let confirm_password : AbstractControl = group.get('confirm_password');

    if(new_password.value === confirm_password.value) {
      return null;
    }

    let error: ValidationError = {'mismatch': true};
    confirm_password.setErrors(error);

    return error;
  }
}
