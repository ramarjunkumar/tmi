var CustomValidators = /** @class */ (function () {
    function CustomValidators() {
    }
    CustomValidators.passwordMatchValidator = function (group) {
        var new_password = group.get('new_password');
        var confirm_password = group.get('confirm_password');
        if (new_password.value === confirm_password.value) {
            return null;
        }
        var error = { 'mismatch': true };
        confirm_password.setErrors(error);
        return error;
    };
    return CustomValidators;
}());
export { CustomValidators };
//# sourceMappingURL=custom-validators.js.map