export class Reset {
    constructor(
        public username?: string,
        public password?: string,
        public new_password?: string,
        public confirm_password?: string,
        public companyName?: string,
        public randomNumber?: string
    ) {}
}
