var User = /** @class */ (function () {
    function User(username, password, companyName, randomNumber) {
        this.username = username;
        this.password = password;
        this.companyName = companyName;
        this.randomNumber = randomNumber;
    }
    return User;
}());
export { User };
//# sourceMappingURL=user.model.js.map