var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { ResetpasswordPage } from '../resetpassword/resetpassword';
import CryptoJS from "crypto-js";
import { LoginProvider } from '../../providers/login/login';
import { User } from './user.model';
import { AlertController } from 'ionic-angular';
// import { PayPage } from '../pay/pay';
import { Storage } from '@ionic/storage';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, loginProvider, alertCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loginProvider = loginProvider;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.user = new User('', '');
        this.clientname = navParams.get('clientname');
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.goDashboard = function () {
        this.navCtrl.push(DashboardPage);
    };
    LoginPage.prototype.goResetPassword = function () {
        this.navCtrl.push(ResetpasswordPage);
    };
    LoginPage.prototype.showLogoutAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Invalid username or password',
            buttons: ['OK']
        });
        alert.present();
    };
    LoginPage.prototype.onSubmit = function () {
        var _this = this;
        this.user.companyName = this.clientname;
        this.encrypt();
        this.user.password = this.userpassword;
        this.loginProvider.login(this.user).
            then(function (data) {
            var result = JSON.parse(data);
            _this.companyName = _this.user.companyName;
            _this.username = _this.user.username;
            if (result && result[0].RESULT === "Sucess") {
                _this.randomNumber = result[0].RANDOMNUMBER;
                _this.storage.set('username', _this.username);
                _this.storage.set('companyName', _this.companyName);
                _this.storage.set('randomNumber', _this.randomNumber);
                // this.navigate(this.companyName, this.randomNumber, this.username);
                console.log(_this.storage.get('username'), 1);
                console.log(_this.storage.get('companyName'), 2);
                console.log(_this.storage.get('randomNumber'), 3);
                _this.navCtrl.push(DashboardPage);
            }
            else {
                _this.showLogoutAlert();
                _this.user.password = '';
            }
        });
    };
    LoginPage.prototype.encrypt = function () {
        var text = this.user.password;
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        this.userpassword = CryptoJS.enc.Utf8.parse(encryptedlogin);
        // console.log(this.userpassword);
        // console.log(encryptedlogin);
        // alert(this.userpassword);
    };
    LoginPage.prototype.navigate = function (companyName, randumNumber, username) {
        this.navCtrl.push(DashboardPage, {
            client: companyName,
            randumnum: randumNumber,
            name: username
        });
    };
    LoginPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-login',
            templateUrl: 'login.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            LoginProvider,
            AlertController,
            Storage])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map