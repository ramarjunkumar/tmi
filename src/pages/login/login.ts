import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { ResetpasswordPage } from '../resetpassword/resetpassword';
import CryptoJS from "crypto-js";

import { LoginProvider } from '../../providers/login/login';
import { User } from './user.model';
import { AlertController } from 'ionic-angular';
// import { PayPage } from '../pay/pay';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = new User('', '');
  public clientname: string;
  public companyName: string;
  public username: string;
  public randomNumber: string;
  public userpassword: string;
  public encpassword: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loginProvider: LoginProvider,
    public alertCtrl: AlertController,
    private storage: Storage,
    public loadingCtrl: LoadingController) {
      this.clientname = navParams.get('clientname');
  }

  ngOnInit() {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goDashboard() {
  	this.navCtrl.push(DashboardPage);
  }

  goResetPassword() {
    this.navCtrl.push(ResetpasswordPage);
  }

  showLogoutAlert() {
    const alert = this.alertCtrl.create({
      title: 'Invalid username or password',
      buttons: ['OK']
    });
    alert.present();
  }

  onSubmit(): void {
    let loader = this.loadingCtrl.create({
    });
    this.user.companyName = this.clientname;
    this.encrypt();
    this.user.password = this.userpassword;
    loader.present();
    this.loginProvider.login(this.user).
    then((data: any) => 
    {
      const result = JSON.parse(data);
      this.companyName = this.user.companyName
      this.username = this.user.username
      console.log(result.length);

      if (result && result.length === 0) {
        loader.dismiss();
        this.showLogoutAlert();
        this.user.password = '';
      }
      else if (result && result[0].RESULT === "Sucess")  {
        loader.dismiss();
        this.randomNumber = result[0].RANDOMNUMBER
        this.storage.set('username', this.username );
        this.storage.set('companyName', this.companyName );
        this.storage.set('randomNumber', this.randomNumber );
        this.navCtrl.push(DashboardPage);
      }
      else {
        loader.dismiss();
        this.showLogoutAlert();
        this.user.password = '';
      }
    })      
  }

    encrypt(): void {
      var text = this.user.password;
      var key = CryptoJS.enc.Utf8.parse('8080808080808080');
      var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

      var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), 
      key,
      { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
      this.userpassword = CryptoJS.enc.Utf8.parse(encryptedlogin);
    }

    navigate(companyName, randumNumber, username): void{
      this.navCtrl.push(DashboardPage, {
        client: companyName,
        randumnum: randumNumber,
        name: username
      });
    }

}

