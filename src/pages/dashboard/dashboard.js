var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PayPage } from '../pay/pay';
import { TaxPage } from '../tax/tax';
import { CtcPage } from '../ctc/ctc';
import { ProfilePage } from '../profile/profile';
import { HomePage } from '../home/home';
import { LogoutProvider } from '../../providers/logout/logout';
import { User } from '../login/user.model';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
var DashboardPage = /** @class */ (function () {
    function DashboardPage(navCtrl, navParams, logoutProvider, alertCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.logoutProvider = logoutProvider;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.user = new User('', '');
        this.storage.get('username').then(function (val) {
            console.log('Your age is', val);
        });
        this.storage.get('companyName').then(function (val) {
            console.log('Your age is', val);
        });
        this.storage.get('randomNumber').then(function (val) {
            console.log('Your age is', val);
        });
        console.log(this.storage.get('username'), 1);
        console.log(this.storage.get('companyName'), 2);
        console.log(this.storage.get('randomNumber'), 3);
        this.clientname = navParams.get('client');
        this.name = navParams.get('name');
        this.randumnum = navParams.get('randumnum');
        console.log(1, this.clientname, this.name, this.randumnum);
    }
    DashboardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashboardPage');
        console.log(this.randumnum);
    };
    // goPayPage() {
    // 	this.navCtrl.push(PayPage);
    // }
    DashboardPage.prototype.goTaxPage = function () {
        this.navCtrl.push(TaxPage);
    };
    DashboardPage.prototype.goCtcPage = function () {
        this.navCtrl.push(CtcPage);
    };
    DashboardPage.prototype.goProfilePage = function () {
        this.navCtrl.push(ProfilePage);
    };
    DashboardPage.prototype.goHomePage = function () {
        this.navCtrl.push(HomePage);
    };
    DashboardPage.prototype.navigate = function (clientname, name, randumnum) {
        console.log(5, this.clientname);
        this.navCtrl.push(PayPage, {
            clientname: this.clientname,
            randomNumber: this.randumnum,
            username: this.name
        });
    };
    DashboardPage.prototype.navigateCtcPage = function (clientname, name, randumnum) {
        console.log(5, this.clientname);
        this.navCtrl.push(CtcPage, {
            clientname: this.clientname,
            randomNumber: this.randumnum,
            username: this.name
        });
    };
    DashboardPage.prototype.navigateTaxPage = function (clientname, name, randumnum) {
        console.log(5, this.clientname);
        this.navCtrl.push(TaxPage, {
            clientname: this.clientname,
            randomNumber: this.randumnum,
            username: this.name
        });
    };
    DashboardPage.prototype.navigateProfilePage = function (clientname, name, randumnum) {
        console.log(5, this.clientname);
        this.navCtrl.push(ProfilePage, {
            clientname: this.clientname,
            randomNumber: this.randumnum,
            username: this.name
        });
    };
    DashboardPage.prototype.showLogoutOkAlert = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Are you Sure want to logout?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'OK',
                    handler: function () {
                        _this.navCtrl.push(HomePage);
                    }
                }
            ]
        });
        alert.present();
    };
    DashboardPage.prototype.showErrorLogoutAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Failed to logout',
            buttons: ['OK']
        });
        alert.present();
    };
    DashboardPage.prototype.logout = function () {
        var _this = this;
        this.user.companyName = this.clientname;
        this.user.username = this.name;
        this.user.randomNumber = this.randumnum;
        this.logoutProvider.logout(this.user).
            then(function (data) {
            var result = JSON.parse(data);
            if (result && result[0].RESULT === "Sucess") {
                _this.randumnum = result[0].RANDOMNUMBER;
                _this.showLogoutOkAlert();
            }
            else {
                _this.showErrorLogoutAlert();
            }
        });
    };
    DashboardPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-dashboard',
            templateUrl: 'dashboard.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            LogoutProvider,
            AlertController,
            Storage])
    ], DashboardPage);
    return DashboardPage;
}());
export { DashboardPage };
//# sourceMappingURL=dashboard.js.map