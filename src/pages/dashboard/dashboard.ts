import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { PayPage } from '../pay/pay';
import { TaxPage } from '../tax/tax';
import { CtcPage } from '../ctc/ctc';
import { ProfilePage } from '../profile/profile';
import { HomePage } from '../home/home';
import { LoginProvider } from '../../providers/login/login';
import { User } from '../login/user.model';
import { Menu } from './menu.model';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { setTimeout } from 'timers';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage implements OnInit {

  public clientname: string;
  public name: string;
  public randumnum: string
  user = new User('', '');
  public username: string;
  public companyName: string;
  public randomNumber: string;
  menus: Menu[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loginProvider: LoginProvider,
    public alertCtrl: AlertController,
    private storage: Storage,
    public loadingCtrl: LoadingController,
    private platform: Platform) 
  {
    this.storage.get('username').then((val) => {
        this.username=val;
    });
      this.storage.get('companyName').then((val) => {
        this.companyName=val;
    });
      this.storage.get('randomNumber').then((val) => {
        this.randomNumber=val;
    });
    this.clientname = navParams.get('client');
    this.name = navParams.get('name');
    this.randumnum = navParams.get('randumnum');
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.user.companyName = this.companyName;
      this.user.username = this.username;
      this.user.randomNumber = this.randomNumber;
      this.loginProvider.menuControl(this.user).
      then((data: any) => 
      {
        const result = JSON.parse(data);
        console.log(result);
        console.log(result.length);
        if(result){
          this.menus = result;
          console.log(this.menus);
        }
        else {
          console.log('error');
        }
      })
    },1000);      
  }

  ionViewDidLoad() {
    console.log('load');
  }

  ionViewCanEnter() {
    return this.loginProvider.authenticated();
  }

  navigate(companyName, username, randomNumber): void{
    this.navCtrl.push(PayPage, {
      clientname: this.companyName,
      randomNumber: this.randomNumber,
      username: this.username
    });
  }

  navigateCtcPage(companyName, username, randomNumber): void{
    this.navCtrl.push(CtcPage, {
      clientname: this.companyName,
      randomNumber: this.randomNumber,
      username: this.username
    });
  }

  navigateTaxPage(companyName, username, randomNumber): void{
    this.navCtrl.push(TaxPage, {
      clientname: this.companyName,
      randomNumber: this.randomNumber,
      username: this.username
    });
  }

  navigateProfilePage(companyName, username, randomNumber): void{
    this.navCtrl.push(ProfilePage, {
      clientname: this.companyName,
      randomNumber: this.randomNumber,
      username: this.username
    });
  }

  showLogoutOkAlert() {
    let alert = this.alertCtrl.create({
      title: 'Are you sure you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.confirmLogout();
            this.navCtrl.push(HomePage);
          }
        }
      ]
    });
    alert.present();
  }

  showErrorLogoutAlert() {
    const alert = this.alertCtrl.create({
      title: 'Failed to logout',
      buttons: ['OK']
    });
    alert.present();
  }

  confirmLogout() {
    const alert = this.alertCtrl.create({
      title: 'You have logged out',
      buttons: ['OK']
    });
    alert.present();
  }

  logout(): void {
    let loader = this.loadingCtrl.create({
    });
    this.user.companyName = this.companyName;
    this.user.username = this.username;
    this.user.randomNumber = this.randomNumber;
    loader.present();
    let alert = this.alertCtrl.create({
      title: 'Are you sure you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            loader.dismiss();
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.loginProvider.logout(this.user).
            then((data: any) => 
            {
              const result = JSON.parse(data); 
              if (result && result[0].RESULT === "Sucess") {
                this.randumnum = result[0].RANDOMNUMBER;
                this.storage.clear();
                loader.dismiss();
              }
              else {
                this.showErrorLogoutAlert();
                loader.dismiss();
              }
            })   
            this.confirmLogout();
            this.navCtrl.push(HomePage);
          }
        }
      ]
    });
    alert.present();
    loader.dismiss();
  }
}
