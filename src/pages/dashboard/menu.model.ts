export class Menu {
    constructor(
        public username: string,
        public password: string,
        public companyName?: string,
        public randomNumber?: string,
        public S_MENU_DESC?: string
    ) {}
}
