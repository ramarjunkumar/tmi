var Pay = /** @class */ (function () {
    function Pay(month, year, username, randomNumber, companyName, S_DESCRIPTION, S_COMPONENT_TYPE, N_COMPONENT_VALUE, N_SEQUENCE_ID) {
        this.month = month;
        this.year = year;
        this.username = username;
        this.randomNumber = randomNumber;
        this.companyName = companyName;
        this.S_DESCRIPTION = S_DESCRIPTION;
        this.S_COMPONENT_TYPE = S_COMPONENT_TYPE;
        this.N_COMPONENT_VALUE = N_COMPONENT_VALUE;
        this.N_SEQUENCE_ID = N_SEQUENCE_ID;
    }
    return Pay;
}());
export { Pay };
//# sourceMappingURL=pay.model.js.map