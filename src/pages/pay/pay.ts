import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PayProvider } from '../../providers/pay/pay';
import { Pay } from './pay.model';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { setTimeout } from 'timers';
import { DashboardPage } from '../dashboard/dashboard';

@IonicPage()
@Component({
  selector: 'page-pay',
  templateUrl: 'pay.html',
})
export class PayPage {

  pay = new Pay('', '');
  pays: Pay[] = [];
  payYear: any[] = [];
  clientname: string;
  username: string;
  randumnum: string;
  grossearning: string = "grossdeduction";
  public isSubmitted: boolean = false;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private payProvider: PayProvider,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController
            )  {
    this.clientname = navParams.get('clientname');
    this.username = navParams.get('username');
    this.randumnum = navParams.get('randomNumber');
    console.log('pay', this.clientname, this.username, this.randumnum);
    const minYear = 2017;
    const currentYear = new Date().getFullYear();
    for (let i = minYear; i <= currentYear; i++) {
      this.payYear.push(i);
    }
    console.log(this.payYear);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PayPage');
  }

  alert() {
    const alert = this.alertCtrl.create({
      title: 'Please select month and year',
      buttons: ['OK']
    });
    alert.present();
  }

  ionViewWillLeave() {
    console.log('hai');
  }

  test(){
    console.log('test');
  }

  onSubmit(): void {
    let loader = this.loadingCtrl.create({
    });
    this.pay.companyName = this.clientname;
    this.pay.username = this.username;
    this.pay.randomNumber = this.randumnum;
    loader.present();
    this.payProvider.payslip(this.pay).
    then((data: any) => 
    {
      const result = JSON.parse(data);
      if(this.pay.month && this.pay.year ){
        this.pays = result;
        loader.dismiss();
        setTimeout(() => {
          this.isSubmitted = true;          
        }, 2000);
      }
      else {
        this.alert();
        loader.dismiss();
      }
    })      
  }

}
