var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PayProvider } from '../../providers/pay/pay';
import { Pay } from './pay.model';
import { AlertController } from 'ionic-angular';
var PayPage = /** @class */ (function () {
    function PayPage(navCtrl, navParams, payProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.payProvider = payProvider;
        this.alertCtrl = alertCtrl;
        this.pay = new Pay('', '');
        this.pays = [];
        this.grossearning = "grossdeduction";
        this.clientname = navParams.get('clientname');
        this.username = navParams.get('username');
        this.randumnum = navParams.get('randomNumber');
        console.log('pay', this.clientname, this.username, this.randumnum);
    }
    PayPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PayPage');
    };
    PayPage.prototype.alert = function () {
        var alert = this.alertCtrl.create({
            title: 'Please select month and year',
            buttons: ['OK']
        });
        alert.present();
    };
    PayPage.prototype.onSubmit = function () {
        var _this = this;
        this.pay.companyName = this.clientname;
        this.pay.username = this.username;
        this.pay.randomNumber = this.randumnum;
        this.payProvider.payslip(this.pay).
            then(function (data) {
            var result = JSON.parse(data);
            if (_this.pay.month && _this.pay.year) {
                _this.pays = result;
            }
            else {
                _this.alert();
            }
        });
    };
    PayPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-pay',
            templateUrl: 'pay.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            PayProvider,
            AlertController])
    ], PayPage);
    return PayPage;
}());
export { PayPage };
//# sourceMappingURL=pay.js.map