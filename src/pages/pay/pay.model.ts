export class Pay {
    constructor(
        public month: string,
        public year: string,
        public username?: string,
        public randomNumber?: string,
        public companyName?: string,
        public S_DESCRIPTION?: string,
        public S_COMPONENT_TYPE?: string,
        public N_COMPONENT_VALUE?: string,
        public N_SEQUENCE_ID?: string

    ) {}
}
