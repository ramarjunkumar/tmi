import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Profile } from '../../pages/profile/profile.model';

@Injectable()
export class ProfileProvider {

  constructor(public http: HttpClient) { }

   	apiUrl = 'https://www.talentmaximus.info/tmimobileapi/profile?';


  	getProfile(inputdata: Profile) {
			return new Promise(resolve => {
				this.http.get(this.apiUrl, {
					params: {
						'strusername': inputdata.username,
						'strRandom': inputdata.randomNumber,
                        'strCompanyName': inputdata.companyName
					}
				}).subscribe(data => {
					resolve(data);
				}, err => {
					console.log(err);
			});
	  });
	}


}
