import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Reset } from '../../pages/resetpassword/resetpassword.model';

@Injectable()
export class ResetProvider {

  constructor(public http: HttpClient) { }

   	apiUrl = 'https://www.talentmaximus.info/tmimobileapi/Reset?';


  	resetPassword(inputdata: Reset) {
			return new Promise(resolve => {
				this.http.get(this.apiUrl, {
					params: {
						'strusername': inputdata.username,						
						'strRandom': inputdata.randomNumber,
						'strCompanyName': inputdata.companyName,
						'strPassword': inputdata.password,
						'strNewPassword': inputdata.new_password					
					}
				}).subscribe(data => {
					resolve(data);
				}, err => {
					console.log(err);
			});
	  });
	}
}
