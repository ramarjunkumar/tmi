import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../pages/login/user.model';

@Injectable()
export class LogoutProvider {

  constructor(public http: HttpClient) { }
  	private isLoggedIn = false;
   	apiUrl = 'https://www.talentmaximus.info/tmimobileapi/logout?';


  	logout(inputdata: User) {
			return new Promise(resolve => {
				this.http.get(this.apiUrl, {
					params: {
						'strusername': inputdata.username,
						'strRandom': inputdata.randomNumber,
						'strCompanyName': inputdata.companyName						
					}
				}).subscribe(data => {
					resolve(data);
					this.isLoggedIn = false;
				}, err => {
					console.log(err);
			});
	  });
	}
}
