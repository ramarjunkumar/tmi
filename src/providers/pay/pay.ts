import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pay } from '../../pages/pay/pay.model';

@Injectable()
export class PayProvider {

  constructor(public http: HttpClient) { }

   	apiUrl = 'https://www.talentmaximus.info/tmimobileapi/payslip?';


  	payslip(inputdata: Pay) {
			return new Promise(resolve => {
				this.http.get(this.apiUrl, {
					params: {
						'strusername': inputdata.username,
						'strRandom': inputdata.randomNumber,
                        'strCompanyName': inputdata.companyName,
                        'strMonth' : inputdata.month,
                        'strYear' : inputdata.year
						
					}
				}).subscribe(data => {
					resolve(data);
				}, err => {
					console.log(err);
			});
	  });
	}


}
