import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Ctc } from '../../pages/ctc/ctc.model';

@Injectable()
export class CtcProvider {

  constructor(public http: HttpClient) { }

   	apiUrl = 'https://www.talentmaximus.info/tmimobileapi/CTC?';


  	getctc(inputdata: Ctc) {
			return new Promise(resolve => {
				this.http.get(this.apiUrl, {
					params: {
						'strusername': inputdata.username,
						'strRandom': inputdata.randomNumber,
                        'strCompanyName': inputdata.companyName						
					}
				}).subscribe(data => {
					resolve(data);
				}, err => {
					console.log(err);
			});
	  });
	}


}
