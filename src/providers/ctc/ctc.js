var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
var CtcProvider = /** @class */ (function () {
    function CtcProvider(http) {
        this.http = http;
        this.apiUrl = 'https://www.talentmaximus.info/tmimobileapi/CTC?';
    }
    CtcProvider.prototype.getctc = function (inputdata) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl, {
                params: {
                    'strusername': inputdata.username,
                    'strRandom': inputdata.randomNumber,
                    'strCompanyName': inputdata.companyName
                }
            }).subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    CtcProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], CtcProvider);
    return CtcProvider;
}());
export { CtcProvider };
//# sourceMappingURL=ctc.js.map