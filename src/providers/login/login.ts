import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../pages/login/user.model';

@Injectable()
export class LoginProvider {

  constructor(public http: HttpClient) { }
  private isLoggedIn = true;
	   apiUrl = 'https://www.talentmaximus.info/tmimobileapi/login?';
	   logoutApiUrl = 'https://www.talentmaximus.info/tmimobileapi/logout?';
	   menuApiUrl = 'https://www.talentmaximus.info/tmimobileapi/Menu?'
	   


  	login(inputdata: User) {
		return new Promise(resolve => {
			this.http.get(this.apiUrl, {
				params: {
					'strusername': inputdata.username,
					'strPassword': inputdata.password,
					'strCompanyName': inputdata.companyName						
				}
			}).subscribe(data => {
				resolve(data);
				this.isLoggedIn = true;
				
			}, err => {
				console.log(err);
			});
	  	});
	}

	logout(inputdata: User) {
		return new Promise(resolve => {
			this.http.get(this.logoutApiUrl, {
				params: {
					'strusername': inputdata.username,
					'strRandom': inputdata.randomNumber,
					'strCompanyName': inputdata.companyName						
				}
			}).subscribe(data => {
				resolve(data);
				this.isLoggedIn = false;
			}, err => {
				console.log(err);
			});
		});
	}

	authenticated() : boolean {
		return this.isLoggedIn;
	}

	menuControl(inputdata: User) {
		return new Promise(resolve => {
			this.http.get(this.menuApiUrl, {
				params: {
					'strusername': inputdata.username,
					'strRandom': inputdata.randomNumber,
					'strCompanyName': inputdata.companyName						
				}
			}).subscribe(data => {
				resolve(data);
				this.isLoggedIn = true;
			}, err => {
				console.log(err);
			});
		});
	}

}
