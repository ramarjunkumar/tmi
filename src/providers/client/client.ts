import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Client } from '../../pages/home/client.model';

@Injectable()
export class ClientProvider {

  constructor(public http: HttpClient) { }

   	apiUrl = 'https://www.talentmaximus.info/tmimobileapi/client?';


  	clientLogin(inputdata: Client) {
        return new Promise(resolve => {
            this.http.get(this.apiUrl, {
                params: {
                    'strCompany': inputdata.clientcode						
                }
            }).subscribe(data => {
                resolve(data);
            }, err => {
                console.log(err);
        });
	  });
	}


}
