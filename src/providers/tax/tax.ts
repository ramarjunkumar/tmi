import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Tax } from '../../pages/tax/tax.model';

@Injectable()
export class TaxProvider {

  constructor(public http: HttpClient) { }

   	apiUrl = 'https://www.talentmaximus.info/tmimobileapi/TaxSheet?';


  	gettax(inputdata: Tax) {
			return new Promise(resolve => {
				this.http.get(this.apiUrl, {
					params: {
						'strusername': inputdata.username,
						'strRandom': inputdata.randomNumber,
                        'strCompanyName': inputdata.companyName
					}
				}).subscribe(data => {
					resolve(data);
				}, err => {
					console.log(err);
			});
	  });
	}
}
